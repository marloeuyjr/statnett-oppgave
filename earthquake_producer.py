#!/usr/bin/env python3

"""
Author : marloe.uy <marloe.uy.jr@gmail.com>
Date   : 2023-01-11
Purpose: Get data from USGS Earthquake API and send to Kafka.
         Data is filtered and preprocessed.
"""

import json
from confluent_kafka import Producer
import requests
import logging
from datetime import datetime

# Configuration
# configure logging
logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='producer.log',
                    filemode='w')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Config for Kafka
BOOTSTRAP_SERVER = "172.21.0.101:9092"
TOPIC = "earthquake-topic"


def receipt(err, msg):
    """Use as Kafka produce callback function."""
    if err is not None:
        print('Error: {}'.format(err))
    else:
        message = 'Produced message on topic {} with value of {}\n'.format(
            msg.topic(), msg.value().decode('utf-8'))
        logger.info(message)
        print(message)


def get_api_data(url="https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.geojson"):
    """Get data from api endpoint"""
    # Fetch the data from api. Api url default can be replaced.
    response = requests.get(url)
    return response.text


def filter_data(api_data):
    """Function to filter relevant data from API response"""
    api_data = json.loads(api_data)
    filtered_data = []
    for feature in api_data['features']:
        if feature['properties']['mag'] >= 4:
            properties = feature['properties']
            magnitude = properties['mag']
            place = properties['place']
            country = place.split(", ")[-1]
            time = datetime.fromtimestamp(
                properties['time']/1000.0).strftime('%Y-%m-%d %H:%M:%S')
            tsunami = properties['tsunami']
            earthquake = {'magnitude': magnitude, 'place': place,
                          'country': country, 'time': time, 'tsunami': tsunami}           
            filtered_data.append(earthquake)
    return filtered_data


def send_data_to_kafka(producer, topic, data):
    """Function to send data to kafka"""
    for earthquake in data:
        producer.produce(topic, json.dumps(earthquake), callback=receipt)
        producer.flush()


if __name__ == '__main__':

    # Initialize Kafka Producer
    producer = Producer({'bootstrap.servers': BOOTSTRAP_SERVER})
    message = 'Kafka Producer has been initiated...'
    logger.info(message)
    print(message)

    try:
        f = filter_data(get_api_data())

        # Send data to Kafka
        send_data_to_kafka(producer, TOPIC, f)

    except Exception as e:
        print(f"Error: {e}")
