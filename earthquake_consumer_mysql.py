#!/usr/bin/env python3

"""
Author : marloe.uy <marloe.uy.jr@gmail.com>
Date   : 2023-01-11
Purpose: Consume data from Kafka topic "earthquake-topic" and
         store to a mysql database.
"""

from confluent_kafka import Consumer, KafkaError
import json
import mysql.connector
import os
from dotenv import load_dotenv
from pathlib import Path
import logging


# Configuration
# configure logging
logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='consumer_mysql.log',
                    filemode='w')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Config for Kafka
BOOTSTRAP_SERVER = "172.21.0.101:9092"
TOPIC = "earthquake-topic"

# Config to db
dotenv_path = Path('/home/marloe.uy/repos/statnett/mysql.env')
load_dotenv(dotenv_path=dotenv_path)

MYSQL_USER = "mydb_user"
MYSQL_PASSWORD = os.environ.get('MYSQL_PASSWORD')
MYSQL_HOST = "172.21.0.102"
MYSQL_DATABASE = "statnett"


def create_db_connection():
    """Create a mysql connection"""
    return mysql.connector.connect(
        host=MYSQL_HOST,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        database=MYSQL_DATABASE
    )


def save_data_to_mysql(data):
    """Call create_db_connection method and create cursor. Input data is from kafka consumer."""
    connection = create_db_connection()
    cursor = connection.cursor()
    message = "Database connection initiated."
    try:
        logger.info(message)
        print(message)
        # prepare SQL statement
        sql = "INSERT INTO earthquake (magnitude, place, time, country, tsunami) VALUES (%s, %s, %s, %s, %s)"
        cursor.execute(
            sql, (data['magnitude'], data['place'], data['time'], data['country'], data['tsunami']))
        connection.commit()
        logger.info("Committed")
    except mysql.connector.Error as error:
        message = f"Error: {error}"
        logger.error(message)
        print(message)
    finally:
        # close connections
        cursor.close()
        connection.close()


def consume_kafka():
    """Consume from Kafka topic."""
    consumer = Consumer({
        'bootstrap.servers': BOOTSTRAP_SERVER,
        'group.id': 'mygroup',
        'auto.offset.reset': 'earliest'
    })

    # Subscribe to the topic
    consumer.subscribe([TOPIC])

    while True:
        msg = consumer.poll(1.0)

        if msg is None:
            # No message received
            continue
        if msg.error():
            if msg.error().code() == KafkaError._PARTITION_EOF:
                # End of partition event
                message = "Reached end of partition."
                logger.error(message)
            else:
                logger.error(f"Error: {msg.error()}")
        else:
            # Store the message in MySQL
            save_data_to_mysql(json.loads(msg.value()))
            logger.info(f"Stored {msg.value()} in MySQL")


if __name__ == "__main__":
    consume_kafka()